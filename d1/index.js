console.log("Hello World!")

// Javascript can also command our browsers to perform different arithmetic operations just like how mathematics work

// Arithmetic operators section

// creation of variables to use in mathematics operations

let x = 1397;
let y = 7831;


// Basic operators 

/* 
+ addition
- subtraction
*multiplication
/ division
% modulo operator
*/


let sum = x + y;
console.log(sum);

let difference = x-y;
console.log(difference);

let product = x*y;
console.log(product);

let quotient = x/y;
console.log(quotient);

let remainder = y%x;
console.log(remainder);

/* Assignment Operator
 = assignment operator - it is used ot assign a value to a variable; the value on the right side of the operator is assigned to the left number*/
let assignmentNumber = 8;


// addition assignment operator - adds the value of the right operant to a variable and assigns the results to the variable
// assignmentNumber = assignmentNumber + 2;
// shorthand for addition assignment (+=)
assignmentNumber += 2;
console.log(assignmentNumber);


/* shift + optiion + A for multiline comment */

/* shift + optiion + up down for duplcating lines  */

// subtraction operator
assignmentNumber -= 2;
console.log(assignmentNumber);

// multiplication operator
assignmentNumber *= 2;
console.log(assignmentNumber);

// division assignment operator
assignmentNumber /= 2;
console.log(assignmentNumber);

// Multiple Operators and Parenthesis

/* 
When multiple operators are present in a single statement, it follows the PEMDAS (Parenthesis, Exponent, Multiplication, Division, Addiiton, Subtraction)
*/

/* 
MDAS
the code below is computed based on the following:
3*4 = 12
12/5 = 2.4
1 + 2=3
3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); 

/* 
pemdas 
code below is computed based on the ff:
2 -3 = -1
-1 * 4 = -4
-4 / 5 = -.8
4.1 + -.8 = .2
*/

let pemdas = 1 + (2 - 3) * 4 / 5;
console.log(pemdas); 

/* 
adding another set of parenthesis ot create amore complex commputation would still follow the same rule:
4 / 5 = 0.8
2 - 3 = 0.1
1 + -1 = 0
0* 0.8 = 0
*/

pemdas = (1+(2-3))*(4/5);
console.log (pemdas);

// Increment and Decrement Section
// assigning a value to a variable to be used in increment and decrement section

let z = 1;

/* 
increment ++ - adding 1 to the value of the variable whether before or after the assigning of value
    pre-increment - is adding 1 to the value fo the variable before it is assigned to the variable

*/

// the value of z is added by a value of 1 before returning the value and storing it inside the variable

let increment = ++z
// the value of z is added by a value of 1 before returning the value and storing it inside the variable
console.log ("Result of pre-increment: " + increment);
// the value of z was also increased by 1  even though we didn't explicityly specify any value reassignment 
console.log ("Result of pre-increment: " + increment);

increment = z++;
// the value of z is at 2 before it was incremented
console.log("Result of post-increment: " + increment);
// the value of z was increased again reassigning the value to 3
console.log("Result of post-increment: " + z);

/* 
decrement -- subtracting 1 to the value whether before or after assigning it to the variable
    pre-decrement -- subtracting 1 to the value before it is assigned to the variable
    post-increment - subtracting 1 to the value after it is assigned to the variable
*/

let decrement = --z;
//the value of z is at 3 before it was decremented
console.log("Result of pre-decrement: " + increment);
//the value of z was reassigned to 2
console.log("Result of pre-decrement: " + z);

decrement = z--;
// the value of z was 2 before it was decremented
console.log("Result of pre-decrement: " + increment);
//the value of z was decreased and assigned to 1
console.log("Result of pre-decrement: " + z);

// Type Coercion

/* 
the automatic or implicit conversion of values from one data type to another, this happens when operations are performed on different data types that would normally not be possible and yield regular results

values are automatically assigned/converted from one datatype to another in order to resolve operations
*/

let numbA = '10';
let numbB = 12;
/* 
resulting data type is a string so it cannot be included in a mathematical operation
*/
let coercion = numbA + numbB;
console.log(coercion);

/* 
number data + number data equal number data
*/
let numberData = 12 + 12;
console.log(numberData);

// boolean + number equal number data (true is equal to 1, false is equal to 0)
let boolNum = true + 12;
console.log(boolNum);

// boolean + string equal string 
let boolString = true + ' We are';
console.log(boolString);

// num + num 
let numbC = 16;
let numbD = 14;
// non coercion happens when the resulting data type is not different from both of the original data types

let nonCoercion = numbC + numbD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// boolean + num

/* 
boolean is binary in js
true = 1
false = 0
*/

// console.log(nubmbE);
// console.log(typeof numbE);

let varA = "String plus " + true;
console.log(varA);
console.log(typeof varA);


// Comparison Operators

/* Equality operator
check if the operands are equal or have same content
attempts to convert and compare operands of different data types returns boolean value
*/

// true
console.log(1 == 1);
// false
console.log(1 == 2);
// true
console.log(1 == "1");
// true
console.log(1 == true);
// true
console.log("juan" == "juan");

let juan = 'juan';
console.log ('juan' == juan);

// Inequality Operator
// check if the operands are not equal or doesn't have same content
// attempts to convert and compare operands of different data types returns boolean value

// false
console.log(1 != 1);
// true
console.log(1 != 2);
// false
console.log(1 != '1');
// false
console.log(0 != false);
// false
console.log("juan" != 'juan');
// false
console.log('juan' != juan);


// Strict Equality/Inequality Operators

// Strict Equality Operators - same datatype and value

/* 
checks the content of the operands
it also checks/compares the data types of the 2 operands
JS is a loosley typed langues, meaning that the values of the different data types can be stored inside a variable

strict operators are better to be used in most coses to ensure that the data types provided are correct
*/


// true
console.log(1 === 1);
// false
console.log(1 === 2);
// false
console.log(1 === '1');
// false
console.log(0 === false);
// true
console.log("juan" === 'juan');
// true
console.log('juan' === juan);

// Strict inequality operator

// false
console.log(1 !== 1); 
// true
console.log(1 !== 2);
// true
console.log(1 !== '1');
// true
console.log(0 !== false);
// false
console.log("juan" !== 'juan');
// false
console.log('juan' !== juan);

// Relational Operators
/* 
Some comparison operators check whether one value is greater or less than to the other value
just like equality and inequality operators, they return boolean based on the assessment of the two values
*/

let a = 50;
let b = 65;
// GT / greater than > 
let greaterThan = a>b;
// LT / less than <
let lessThan = a<b;
// GTE / greater than or equal to >=
let greaterThanOrEqualTo = a >= b;
//  LTE / less than or equal to <=
let lessThanOrEqualTo = a<=b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

// true - product of a forced coercion to change the string into a number data type
let numStr = "30";
console.log(a > numStr);

// false - since string is not numeric, it was converted to a number and result is NAN (Not a Number), so false
let string = "twenty";
console.log(b >= string);


// Logical Operators - check whether the values of the two or more variables are true/false

let isLegalAge = true;
let isRegistered = false;

// And Operator (&&)
// returns true if all values are true
/* 
    1       2       end result
    true    true    true
    true    false   false
    false   true    false
    false   flase   false
*/

let allRequirementsMet = isLegalAge && isRegistered
console.log("Result of And Operator: " + allRequirementsMet);


// Or Operator (||)
// returns true if one of the values is true

/* 
1       2       end result
true    true    true
true    false   true
false   true    true
false   false   false
*/

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Or Operator: " + someRequirementsMet);


// Not Operator (!)
// returns the opposite of the value

/* 
!true = false
!false = true
*/

let someRequirementsNotMet = !isRegistered;
console.log("Result of Not Operator: " + someRequirementsNotMet);